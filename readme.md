# Lend Me

"Lend Me" is the name we chose for our Social Lending peer to peer mobile application.

## Problematic 

In 2021, the financial and economic crisis has become undeniably risky and threatening for all the people around the world.
Nowadays, with the high interests suggested by the banks, it has become hard for people to get a loan without having a financial damage on their portfolio. Add to that, only people with convincing loan purposes and stable incomes are allowed to have a bank loan. Thus, having a bank loan has become not only hard but also inaccessible by a certain number of people certainly with the impact made by the COVID-19 pandemic as many global citizens have lost their jobs. The statistics below show the job losses acceleration made by the COVID-19 pandemic: 

![Economic Hit](readme-img/job-losses.png)

Moreover, some people might sometimes be in need of a quick loan in order to solve a problem or start a project which is unfortunately not provided by banks as the process can be very slow and tiring with the papers and details the bank asks for in order to provide a loan to the client.

Actually, the problems related to bank loans aren't just financial but also social. While asking for a loan from a bank, it's very hard to find a bank with the needed guidance and expertise to help the client make their strategies and decisions since the receptionists and loan experts are usually firm and concise. These factors make it hard for the client to understand the status and the details related to his loan specifically and to his financial status generally.

On the other hand, investors find difficulties in investing their money during crisis and hard times since the decrease of investments between 2019 and 2021 has been huge as the statistics below:

![Investments](readme-img/investments.PNG)


That's why, finding new ways to invest money has become essential in order to make growth on the financial domain.


## Our "Lend Me" solution
Our solution consists in creating a social lending, peer to peer mobile application. Our mobile application connects borrowers directly to investors. Initially, we set the rates and the terms. Thus, the borrowers and lenders will be able to make transactions between them using Blockchain and Machine Learning technologies.


## "Lend Me" Features

This repository contains a package which is able to perform the tasks below:

- Creating an account and choosing whether the user is a borrower or lender
- Creating loan requests by borrowers
- Investing loans by lenders
- Entering all the user financial details by specifying the interest, the monthly payment and the loan purpose
- Signing in and out of the Lend Me application
- Matching the lender to the borrower according to their entered financial details such as countries and interest rates
- Analyzing the activities done by each borrower or lender and dislaying them into a real time dashboard
- Recommending the strategies and investments ideas to each borrower and lender
- Making financial transactions between borrowers and lenders using different payment ways
- Ensuring the security and the trust between the borrower and lender during the money transaction
- Accepting or declining the loan requests
- Assigning a score to each borrower and lender according to their respect of deadlines and level of commitment
- Attributing the possibility to loyal clients to borrow or lend a huge amount of money 
- Signing a trust contract between lender and borrower after approving to deal together 
- Watching the history of the loan


## Technologies and architecture used 
Our Social Lending mobile application is created on 6 phases:
1. Creating the React Native user interfaces
2. Working on the borrower-lender Machine Learning matching algorithm using K-Means Clustering to cluster the dating profiles with one another
3. Using Hyper Ledger the open source Blockchain technology in order to store transactions into smart contracts
4. We began architecting a model using Keras, a NN library built on TensorFlow. We iterated through many different Keras architectures by employing different combinations of hidden layer sizes, dropout regularization, L2 kernel regularization, early stopping, epochs, class weight balancing, and ReLU/Sigmoid activation functions
5. Working on the recommendation system using the Machine Learning Random forest algorithm of investments
6. Working on a profile matching technique using machine learning NLP algorithm in order to find the suitable lender for the borrower

## What our code was written in 

- Node.js version 8.10.0
- npm 
- React Native version 0.55.3 
- React version 16.3.0-alpha.2 
- Machine Learning: Recommendation System algorithm
- Flask
- Hyper Ledger for Blockchain 

## Used Libraries
* Moment.js version 2.21.0
* React Native Bcrypt version 2.4.0
* React Native Blur version 3.2.2
* react-native-datepicker version 1.6.0
* React Native Elements version 0.19.0
* React Native Firebase version 4.3.8
* React Native Image Picker version 0.26.7
* react-native-linear-gradient version 2.4.0
* react-native-snap-carousel version 3.7.0
* react-native-svg version 6.3.1
* React Native Timeline Listview version 0.2.3
* React Native Vector Icons version 4.5.0
* React Navigation version 3.2.0
* Victory Native version 0.17.4
* Hyper Ledger
* Flask
* Python
* Hyper Ledger
* Keras Neural Networks
* TensorFlow


## The Blockchain module
Find below the schema describing the Blockchain process in our application: 


![Blockchain](readme-img/block.png)


## What makes our "Lend me" mobile application cool?
* The fact it's a mobile application make it easy for borrowers and lenders to make transactions 
* It makes communication between borrowers and lenders easy and with simple agreements from the beginning
* It makes you avoid dealing with banks and their long processes
* Real time data insights and analysis are always available
* An opportunity for investors to help borrowers and find new ways to invest in their money
* An easy way to borrow a big or small amount of money
* Possibility to people with instable and weak incomes to get loans
* The recommendation related to interests and loans
## "Lend Me" WoW factor
What's WoW about our "Lend Me" application is that it allows investments for all the people without restriction no matter how much money they are willing to invest on. Add to that, it gives the borrower and lender the possibility to discuss the interests without setting obligations in their deal since they can negotiate together. Moreover, the fact we're setting a trust score rate gives the user the chance to increase the money a user can invest in or borrow. Also, before the loan is approved by both parties, the application recommends to you the interests, payments periods and investments you can use to pay by analyzing the details and activities provided by borrowers and lenders due to its machine learning impelemented algorithms using data analytics: Analyzing borrower and lender activities to calculate risk factors and confidence metrics. Using secure and innovative technology such as BLOCKCHAIN makes our application different and with more smart functionnalities than any other P2P app. Without forgetting, that our application matches profiles of borrowers and lenders based on Machine Learning algorithms.

## Why "Lend Me" solution is a good plan for FIS?
- FIS doesn't have solutions related to P2P lending area so it would be important to take its part on the market in this domain
- The Peer to Peer worldwide Lending Market 2021-2027 research report is a valuable source of insightful data for business strategists. It provides the industry overview with growth analysis, revenue, demand, challenges and opportunities
- Many countries are still not investing on the culture of the P2P lending solutions, that's why FIS can use its place and value on the market and start working on the Peer to Peer lending solutions
- Building such application doesn't take a lot of time since there are many open source technologies in the market that can help 
- According to the report published by Allied Market Research, the global P2P payment market generated $1,889.16 billion in 2020, and is estimated to garner $9,097.06 billion by 2030, witnessing an increase of 17.3% from 2021 to 2030. Check the chart below: 
![peertopeer](readme-img/p2p.png)

## Why "Lend Me" solution is different from other solutions?
- Using innovative technologies like Blockchain and Machine Learning together
- Implementing recommendation and Machine Learning algorithms wich is new to the market 
- It's a mobile application which makes it easy and accesible to all smart phone users 
- Our applciation makes it easy for investors to invest from small amount of money as 10$ to a huge amount of money
- Our application is flexible due to the negotiation and suggested recommendation ideas, no firm or strict laws as long as both parties are on the same page  
- Makes it easy for the user to decide and choose the right lender or borrower based on data analysis 


## References

- https://www.investopedia.com/terms/p/peer-to-peer-lending.asp
- https://dzone.com/articles/blockchain-solutions-how-to-transform-your-busines
- https://www.geeksforgeeks.org/k-means-clustering-introduction/

## Author

- Ameni Charradi : charradi.ameni@gmail.com
